// alert('Hello World')

let posts = []; //Mock Database
let count = 1; //Post Id


// Add post
document.querySelector('#form-add-post')
    .addEventListener('submit',(e) => {
        e.preventDefault()
        let title = document.querySelector('#txt-title');
        let body = document.querySelector('#txt-body');
        // console.dir(e)
        posts.push({
            id: count,
            title: title.value,
            body: body.value
        })

        count++
        // console.log(posts)
        // alert('Successfully added')
        title.value = '';
        body.value = '';
        showPosts()
    })


const showPosts = () => {
    let postEntries = '';

    posts.forEach((post) => {
        postEntries += `
            <div id='post-${post.id}'>
                <h3 id='post-title-${post.id}'>${post.title}</h3>
                <p id='post-body-${post.id}'>${post.body}</p>
                <button onClick='editPost(${post.id})'>Edit</button>
                <button onClick='deletePost(${post.id})'>Delete</button>
            </div>
        `
    });

    console.log(postEntries)

    document.querySelector('#div-post-entries').innerHTML = postEntries

}


const editPost = (id) => {
    
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
}

// Update Post
document.querySelector('#form-edit-post')
    .addEventListener('submit',(e) => {
        e.preventDefault()

        for(let i = 0; i < posts.length; i++){
            if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
                posts[i].title = document.querySelector('#txt-edit-title').value;
                posts[i].body = document.querySelector('#txt-edit-body').value;

                showPosts();
                alert('Updated')
                break;
            }


        }



    })


const deletePost = (id) => {

    posts.splice(id - 1,1);
    document.querySelector(`#post-${id}`).remove()
    // OR
    // showPosts();

}
